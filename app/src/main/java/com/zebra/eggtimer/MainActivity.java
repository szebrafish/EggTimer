package com.zebra.eggtimer;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textViewTime;
    Button buttonStartStop;
    SeekBar seekBarTime;

    MediaPlayer mediaPlayer;

    int timerMaxMinutes = 20;
    int currentTime = 600;

    final Handler handler = new Handler();

    Runnable run = new Runnable() {
        @Override
        public void run() {

            seekBarTime.setProgress(currentTime - 1);

            if (currentTime == 0){

                buttonStartStop.setText("Start");
                mediaPlayer.start();

            }
            else{

                handler.postDelayed(this, 1000);
            }

        }
    };

    public void clickButton(View view){

        if (buttonStartStop.getText() != "Pause")
        {
            buttonStartStop.setText("Pause");
            handler.post(run);
        }
        else
        {
            buttonStartStop.setText("Start");
            handler.removeCallbacks(run);
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewTime = (TextView) findViewById(R.id.textViewTime);
        buttonStartStop = (Button) findViewById(R.id.buttonStartStop);

        mediaPlayer = MediaPlayer.create(this, R.raw.alarm);

        seekBarTime = (SeekBar) findViewById(R.id.seekBarTime);
        seekBarTime.setMax(60 * timerMaxMinutes);
        seekBarTime.setProgress(currentTime);

        seekBarTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                //int minutes = (int) progress / 60;
                String minutes = Integer.toString((int) progress / 60);
                //int seconds = progress % 60;
                String seconds = String.format("%02d", progress % 60);


                textViewTime.setText(minutes.concat(":").concat(seconds));

                currentTime = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}
